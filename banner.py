def banner_text(text=" ", screen_width=60):
    """ This function creates a welcoming banner. The Parameters are for strings and screenwidth """
    if len(text) > screen_width - 4:
        raise ValueError("String {0} is larger than specified width {1}"
                        .format(text, screen_width))

    if text == "*":
        print("*" * screen_width)
    elif text == '<->':
         print('<->' * screen_width)      
    else:
        output_string = "**{0}**".format(text.center(screen_width - 4))
        print(output_string)


# banner_text("*")
# banner_text("Always look on the bright side of life...")
# banner_text("If life seems jolly rotten,")
# banner_text("There's something you've forgotten!")
# banner_text("And that's to laugh and smile and dance and sing,")
# banner_text(" ")
# banner_text("When you're feeling in the dumps,")
# banner_text("Don't be silly chumps,")
# banner_text("Just purse your lips and whistle - that's the thing!")
# banner_text("And... always look on the bright side of life...")
# banner_text("*")


def pennywise_lyrics(string):
    screen_width = 80
    if len(string) > screen_width - 4:
        print('The text is too long!')

    if string =='*':
        print('*' * screen_width)
    else:
        output_string = '**{0}**'.format(string.center(screen_width - 4))
        print(output_string)


# pennywise_lyrics('*')
# pennywise_lyrics("Pennywise - Unknown Road")
# pennywise_lyrics("So you're currently content with your surroundings")
# pennywise_lyrics("You possess a vague sense of accomplishment")
# pennywise_lyrics('Did you give all you had to give or did you')
# pennywise_lyrics('give conservative')
# pennywise_lyrics('Do you think that all the years that passed you')
# pennywise_lyrics("by we're all well spent")
# pennywise_lyrics('*')
